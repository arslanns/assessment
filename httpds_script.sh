
hostname=$1
key=$2

ssh -o StrictHostKeyChecking=no -i ~/.ssh/$key ec2-user@$hostname '

sudo yum update

sudo yum install -y httpd.x86_64

sudo systemctl start httpd.service

sudo systemctl enable httpd.service

case $1 in
    'start')
            echo -e "<html>\nHello there<br>" >/var/www/html/index.html
		echo -e "My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')\n</html>" >>/var/www/html/index.html
		echo -e "Operating system $(uname)" >>/var/www/html/index.html
		echo -e "Web server is apache and version $(httpd -v)" >>/var/www/html/index.html
            ;;
    *)
        echo "Not a valid argument"
        ;;
esac
sudo systemctl restart httpd.service

'
