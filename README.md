### Creating and running webservers with a load balancer

## Launching Instances

# Step 1: Logging in

To begin you must set up instances of your machines on the cloud.
You can achieve this by using AWS.

1. To access the AWS website you must log in. Access to the log in page can be found here (https://automationlogic.atlassian.net/wiki/spaces/AWS/pages/22052876/AWS), You must click the first link under 'Log in to the Console'

2. Once on the page you will either be prompted to log in, or already logged in with a previous profile. If you require a new profile get in contact with your organisation. If already logged in click on the top righ drop down arrow and ensure you are in the correct role.

# Step 2 Creating AWS Linux:

3. Search ec2 on the search bar next to services on the top left, and launch the option that states 'Virtual Servers in the cloud'.

4. Click 'launch instances' to begin creating a machine. When prompted to choose a machine begin by selecting the first option of 'Amazon Linux 2 ...'

5. Ensure you have selected the option that has 1 vCPU and 1 Memory, and they click on 'Next: Configure Instance Details'

6. For Configure Instance Details ensure 'Auto-assign Public IP' is enabled, and leave everything else as it is and click 'Next: Add Storage'.

7. You dont have to do anything on this page so just click 'Next: Add Tags'

8. For better management, you mush add a Tag. To do this click on 'Add Tag', Under 'Key' type Name, and under 'Value' type yourfirstname-aws. Then click on 'Next: Congifure Security Group'.

9. Enter an appropriate security group name and description. You should have 'SSH' under type already, under source slected 'My IP'. Then click 'Add Rule' and select HTTP, ensure that the source is 'Anywhere'. Click 'Add Rule' again and select HTTPS, snure that you have also selected 'Anywhere' for this. Give appropriate descriptions for each rule and click 'Review and Launch'.

10. Check all detials are correct and click 'Launch'. Once prompted, create a new key pair and keep it safely stored or use an existing key pair that has been used for previous instances.

11. You have successfully launched an AWS linux machine. Now you must launch 2 Ubuntu machines.

# Creating Ubuntu Machine for Webserver

12. Search ec2 on the search bar next to services on the top left, and launch the option that states 'Virtual Servers in the cloud'.

13. Click 'launch instances' to begin creating a machine. When prompted to choose a machine search 'Ubuntu' on the search bar an select the first option of 'Ubuntu Server 20.04 ...'.

14. Ensure you have selected the option that has 1 vCPU and 1 Memory, and they click on 'Next: Configure Instance Details'

15. For Configure Instance Details ensure 'Auto-assign Public IP' is enabled, and leave everything else as it is and click 'Next: Add Storage'.

16. You dont have to do anything on this page so just click 'Next: Add Tags'

17. For better management, you mush add a Tag. To do this click on 'Add Tag', Under 'Key' type Name, and under 'Value' type yourfirstname-ubuntu, Then click 'Add another tag' and type Project under 'Key' and assessment1 under 'Value'. Then click on 'Next: Congifure Security Group'.

18. Enter an appropriate security group name and description. You should have 'SSH' under type already, under source slected 'My IP'. Then click 'Add Rule' and select HTTP, ensure that the source is 'Anywhere'. Click 'Add Rule' again and select HTTPS, snure that you have also selected 'Anywhere' for this. Give appropriate descriptions for each rule and click 'Review and Launch'.

19. Check all detials are correct and click 'Launch'. Once prompted, create a new key pair and keep it safely stored or use an existing key pair that has been used for previous instances.

20. You have successfully launched the ubuntu server, now you must launch another for the load balancer. To so repeat all steps from 12 - 19, ensuring that the Name tag as the value of yourfirstname-lb.

## Running Scripts

1. To view instances created you will need to select Instances under Instances from the list on the left hand side.

2. # Using bash script to install website
## Prerequisites
* You have cloned this repository using:

```git clone git@bitbucket.org:arslanns/assessment.git```

* Have access to cloud computers and their public ip, at least one for loadbalancing and one for your webserver

## Instructions

### 1. Make sure you are located in the directory of this repository

``` $ cd path/to/repo```

### 2. Type out the following command with your arguments
```bash
# Run the bashscipt with the arguments of the ssh key, the ip of your load balancing computer, and your ips of your webservers seperated by a space
$ launch_script.sh  <loadBalancerIp> <webServerIp1> ... <webServerIpn> <key>
```





