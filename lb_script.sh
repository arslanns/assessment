if [ $# -lt 3 ]
then
    echo "Expected at least 3 argument, $# was given instead"
    exit 1
fi

key=$1
loadbalancer=$2
hostname=$3
hostname1=$4


configline1=""
configline2=""
# Gets the private ip of the webservers and generates config lines for each
private_ip=$(ssh -i ~/.ssh/$key ubuntu@$hostname "hostname -I" 2>&1| xargs)
configline1+="server node$serverNo $private_ip:80 check"

private_ip=$(ssh -i ~/.ssh/$key ec2-user@$hostname1 "hostname -I" 2>&1| xargs)
configline2+="server node$serverNo $private_ip:80 check"

# installs haproxy on our load balancer if it isn't already installed
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$key ubuntu@$loadbalancer '
sudo apt update
sudo apt install haproxy -y
'



# Writes config file including our backend ips to load balancing computer
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$key ubuntu@$loadbalancer  << EOF
sudo bash -c 'echo "
frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
" >> /etc/haproxy/haproxy.cfg'
sudo service haproxy restart
EOF